<?php

class OE_Tool {

/**
     * @param $message
     */
    public static function exitWithError($message) {
        header('HTTP/1.1 503 Service Temporarily Unavailable');
        die($message);
    }
}